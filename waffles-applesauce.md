# Waffles with Applesauce

### Ingredients

 * 3 frozen waffles
 * 3 spoonfuls applesauce

### Instructions

 * reheat waffles in toaster
 * apply a spoonful of applesauce to each waffles

### Info

**Produces**: 1 light meal

**Prep suitability**: ???

**Portability**: Poor, the applesauce is messy and would need to be kept separate, though no utensil are required since the waffles could be dipped

**Costs**: ???

**Takes**: ???

**Source**: ???

**Used in**: nothing
