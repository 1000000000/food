# Chickpea Batter

### Ingredients

 * 1 cup + 2 tbsp besan flour
 * 2 tsp ground flax meal
 * 1 tsp baking soda
 * 1 cup vegan milk
 * 2 tsp apple cider vinegar

### Instructions

 * combine all dry ingredients in bowl
 * combine wet ingredients, mix, and let sit for 10 minutes
 * meanwhile mix the dry ingredients
 * mix wet and dry ingredients

### Info

**Produces**: ???

**Prep suitability**: good, the recipe scales well, and it can be refrigerated, it probably can't be frozen

**Portability**: N/A

**Costs**: ???

**Takes**: ???

**Source**: [Fit Foodie Nutter](https://fitfoodienutter.com/recipe/chickpea-waffles-gf-vegan/)

**Used in**:

 * [Chickpea Waffles](besan-waffles.md)
