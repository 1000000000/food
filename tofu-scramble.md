# Tofu Scramble

### Ingredients

 * 1 block tofu
 * 1/4 tsp salt
 * 1/4 tsp cumin
 * 1/4 tsp onion powder
 * 1/4 tsp tumeric
 * pepper to taste
 * olive oil for pan

### Instructions

 * heat pan with olive oil to sizzle
 * crumble tofu over pan
 * increase pan heat to level 6
 * add salt and spices, and stir until evenly distributed
 * cook until pan is no longer wet

### Info

**Produces**: ???

**Prep suitability**: lack a pan to scale, reheats well, freezing needs to be tested

**Portability**: N/A

**Costs**: ???

**Takes**: ???

**Source**: [Nora Cooks](https://www.noracooks.com/tofu-scramble/)

**Used in**: nothing
