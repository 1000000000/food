# Scallion Cheddar Cheeze Spread

### Ingredients

 * 2 cups cashews
 * 1 cup nutritional yeast
 * 1 cup [vegan mayo](mayo.md)
 * 1 block tofu
 * 1/2 cup vegan milk
 * bundle of scallions
 * salt and pepper to taste

### Instructions

 * Soak cashews in hot water for at least 15 minutes
 * Blend ingredients

### Info

**Produces**: ???

**Prep suitability**: good, but probably doesn't freeze

**Portability**: N/A

**Costs**: ???

**Takes**: ???

**Source**: *Two Dollar Radio Guide to Vegan Cooking*, pg 14

**Used in**: nothing
