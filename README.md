A collection of recipes I've found for daily cooking or special occastions

## Recipes

### Useful ingredients

 * [Mayonnaise](mayo.md)
 * [Scallion Cheddar Cheeze Spread](scallion-cheddar.md)

### Intermediate Stages

 * [Chickpea Batter](besan-batter.md)

### Meal bases

 * [Tofu Scramble](tofu-scramble.md)
 * [Chickpea Waffles](besan-waffles.md)

### Meals

 * [Avocado Toast with Nooch](avocado-toast-nooch.md)
 * [Waffles with Applesauce](waffles-applesauce.md)
