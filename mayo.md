# Mayonnaise

### Ingredients

 * 1 cup aquafaba
 * 3 tbsp lemon juice
 * 1 tsp salt
 * 1 tsp pepper
 * 1 1/4 cup tapioca starch
 * 3 cups olive oil
 * 1 tbsp apple cider vinegar
 * 7 cloves garlic
 * 2 tsp dill
 * 2 tsp onion powder

### Instructions

 * Blend all ingredients
 * Add more aquafaba or tapioca starch to attain desired consistency

### Info

**Produces**: ???

**Prep suitability**: good, but probably doesn't freeze

**Portability**: N/A

**Costs**: ???

**Takes**: ???

**Source**: *Two Dollar Radio Guide to Vegan Cooking*, pg 114

**Used in**:

 * [Scallion Cheddar Cheeze Spread](scallion-cheddar.md)
