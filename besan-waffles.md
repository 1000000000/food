# Chickpea Waffles

### Ingredients

 * 1 cup [chickpea batter](besan-batter.md)
 * coconut oil for waffle iron

### Instructions

 * brush waffle iron with coconut oil and pour on a cup of batter
 * repeat until batter is used up

### Info

**Produces**: 4 waffles

**Prep suitability**: excellent, the recipe scales well and freezes well

**Portability**: N/A

**Costs**: ???

**Takes**: ???

**Source**: [Fit Foodie Nutter](https://fitfoodienutter.com/recipe/chickpea-waffles-gf-vegan/)

**Used in**: nothing
