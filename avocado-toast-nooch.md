# Avocado Toast with Nooch

### Ingredients

 * two slices of bread
 * 1/4 avocado
 * small pile of nutritional yeast
 * dribble of tamari

### Instructions

 * toast bread
 * spread avocado on one of the toasts
 * pour nutritional yeast onto the avocado in a pile
 * make an indentation on top of the pile, and pour in tamari
 * spread the nutritional yeast/tamari mixture across the avocado
 * close into sandwich

### Info

**Produces**: ???

**Prep suitability**: ???

**Portability**: excellent, does not require utensils, can be eaten cold, and is unlikely to make a mess

**Costs**: ???

**Takes**: ???

**Source**: self

**Used in**: nothing
